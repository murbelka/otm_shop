class Product < ActiveRecord::Base
  belongs_to :category
  has_and_belongs_to_many :sizes
  has_many :images
  has_many :cart_items
end
