class CartItemsController < ApplicationController



	def create
		@cart = current_cart

		unless @cart.cart_items.exists?(
				:product_id => params[:cart_item][:product_id], 
				:size_id => params[:cart_item][:size_id])
 			@cart_item = @cart.cart_items.new(cart_item_params)
 		else
 			@cart_item = @cart.cart_items.where(
 					:product_id => params[:cart_item][:product_id], 
 					:size_id => params[:cart_item][:size_id]
 				).first
 			@cart_item.quantity += params[:cart_item][:quantity].to_i
 			@cart_item.save
 		end

 		@cart.save
	  	session[:cart_id] = @cart.id
		# product = Product.find(params[:id])

		# CartItem.new({
		# 		:cart => cart,
		# 		:product => product,
		# 		:quantity => params[:quantity],
		# 		:size => Size.find(params[:size_id]),
		# 		:price => product.price
		# 	}).save
		# redirect_to product_url(params[:id])
	end

	def update
      @cart = current_cart
      @cart_item = @cart.cart_items.find(params[:id])
      @cart_item.update_attributes(cart_item_params)
      @cart_items = @cart.cart_items
  	end

  	def destroy
      @cart = current_cart
      @cart_item = @cart.cart_items.find(params[:id])
      @cart_item.destroy
      @cart_items = @cart.cart_items
    end
private
  def cart_item_params
    params.require(:cart_item).permit(:quantity, :product_id, :size_id)
  end
end
