class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :load_categories, :have_categories
  helper_method :current_cart

  def current_cart
  	if !session[:cart_id].nil?
	  Cart.find(session[:cart_id])
	else
	  Cart.new			
	end
  end
    

  def load_categories
    @categories = Category.all
  end

  def have_categories
    @have_categories = true
  end
end
