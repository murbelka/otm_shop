class CreateProductsSizes < ActiveRecord::Migration
  def change
    create_table :products_sizes do |t|
      t.references :size, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
    end
  end
end
