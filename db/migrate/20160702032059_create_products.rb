class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :category_id
      t.string :title
      t.text :description
      t.decimal :price
      t.boolean :is_voluntary
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
